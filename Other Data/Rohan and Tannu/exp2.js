function Startprograms() {
    document.getElementById("code_para").style.display="inline";
    document.getElementById("flowchart_div").style.display="inline";
   document.getElementById("start_bt").disabled = true;
   document.getElementById("Next").disabled = false;
   const ArrayofLine = document.getElementsByClassName('line');
  
   let inde = -1;

   Next.addEventListener('click', function() {
       inde++;
       ArrayofLine[inde].classList.add('line-highlight');
     
       if (inde > 0) ArrayofLine[inde - 1].classList.remove('line-highlight'
	   );
       if (inde == 0) {
         
         document.getElementById("start").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " Declaring Header file of C";
       
       }
       if (inde == 1) {
         document.getElementById("start").style.fill="#FFDB58";
         document.getElementById("Prev").disabled = false;
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " Here, we start the main function";
       }
       if (inde == 2) {
            document.getElementById("start").style.fill="#FFDB58";
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Opening braces ";
       }
       if (inde == 3) {
            document.getElementById("start").style.fill="#FFDB58";
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Variable a , b holds the value taken from the user. ";
       }
       if (inde == 4) {
         document.getElementById("start").style.fill="#8BE5AD";
         document.getElementById("f").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " To display the first output we use printf";
       }
       if (inde == 5) {
         document.getElementById("start").style.fill="#8BE5AD";
         document.getElementById("f").style.fill="#FFDB58";
        
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " To read the first input from user we use scanf ";
       }
       if (inde == 6) {
         document.getElementById("start").style.fill="#8BE5AD";
         document.getElementById("f").style.fill="#8BE5AD";
         document.getElementById("s").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " Then we display the second output";
       }
       if (inde == 7) {
         document.getElementById("start").style.fill="#8BE5AD";
         document.getElementById("f").style.fill="#8BE5AD";
         document.getElementById("s").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " To read the second input from user we use scanf  ";
       }
       if (inde == 8) {
         document.getElementById("start").style.fill="#8BE5AD";
         document.getElementById("f").style.fill="#8BE5AD";
         document.getElementById("s").style.fill="#8BE5AD";
         document.getElementById("sum").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " Variable sum holding the result of a+b ";
       }
       if (inde == 9) {
         document.getElementById("start").style.fill="#8BE5AD";
         document.getElementById("f").style.fill="#8BE5AD";
         document.getElementById("s").style.fill="#8BE5AD";
         document.getElementById("sum").style.fill="#8BE5AD";
         document.getElementById("print_Sum").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " Again, we are using printf to display the output";
       }
       if (inde == 10) {
         document.getElementById("start").style.fill="#8BE5AD";
         document.getElementById("f").style.fill="#8BE5AD";
         document.getElementById("s").style.fill="#8BE5AD";
         document.getElementById("sum").style.fill="#8BE5AD";
         document.getElementById("print_Sum").style.fill="#8BE5AD";
         document.getElementById("end").style.fill="#FFDB58";
         document.getElementById("Output2").innerHTML = "";
         document.getElementById("Output2").innerHTML = " Closing braces ";
       }

       if(inde== 11)

       {
         swal ( { title:"The code has ended now move to the output section",
                   text: "Click on Run button", 
                   icon: "success",
                   closeOnClickOutside: false

       })
                
       document.getElementById("end").style.fill="#8BE5AD";
         document.getElementById("run").style.display="inline";

         document.getElementById("Next").disabled = true;

         document.getElementById("Prev").disabled = true;

       }
       
   });

   Prev.addEventListener('click', function() {
       inde--;
       ArrayofLine[inde].classList.add('line-highlight');
 
       if (inde >= 0) ArrayofLine[inde + 1].classList.remove('line-highlight');
       if (inde == 0) {
         document.getElementById("start").style.fill="#FFDB58";
         document.getElementById("Output2").innerHTML = "";
         document.getElementById("Output2").innerHTML = " Declaring Header file of C";
         document.getElementById("Prev").disabled = true;
       }
       if (inde == 1) {
         document.getElementById("start").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " Here, we start the main function";
       }
       if (inde == 2) {
         document.getElementById("start").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " Opening braces "; 
       }
       if (inde == 3) {
         document.getElementById("start").style.fill="#FFDB58";
         document.getElementById("f").style.fill="#8BE5AD";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " Variable a , b holds the value taken from user and variable sum holds the result";
       }
       if (inde == 4) {
         document.getElementById("start").style.fill="#8BE5AD";
         document.getElementById("f").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " To display the first output we use printf";
       }
       if (inde == 5) {
         document.getElementById("s").style.fill="#8BE5AD";
         document.getElementById("f").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " To read the first input from user we use scanf";
       }
       if (inde == 6) {
         document.getElementById("sum").style.fill="#8BE5AD";
         document.getElementById("f").style.fill="#8BE5AD";
         document.getElementById("s").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " Then we display the second output";
       }
       if (inde == 7) {
         document.getElementById("sum").style.fill="#8BE5AD";
         document.getElementById("f").style.fill="#8BE5AD";
         document.getElementById("s").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " To read the second input from user we use scanf";
       }
       if (inde == 8) {
         document.getElementById("print_Sum").style.fill="#8BE5AD";
         
         document.getElementById("sum").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " Variable sum is holding the result of a+b";
       }
       if (inde == 9) {
         document.getElementById("end").style.fill="#8BE5AD";
    
         document.getElementById("print_Sum").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " Again, we are using printf to display the output ";
       }
       if (inde == 10) {
         document.getElementById("start").style.fill="#8BE5AD";
         document.getElementById("f").style.fill="#8BE5AD";
         document.getElementById("s").style.fill="#8BE5AD";
         document.getElementById("sum").style.fill="#8BE5AD";
         document.getElementById("print_Sum").style.fill="#8BE5AD";
         document.getElementById("end").style.fill="#FFDB58";
           document.getElementById("Output2").innerHTML = "";
           document.getElementById("Output2").innerHTML = " Closing braces ";
       }
       
   });
}


// Run CODE

function run()
{
   document.getElementById("first_no_label").style.display="inline";
   document.getElementById("first").style.display="inline";
   document.getElementById("second_no_label").style.display="inline";
   document.getElementById("second_no_label1").style.display="inline";
   document.getElementById("second").style.display="inline";
   document.getElementById("execute").disabled=false;
   document.getElementById("run").disabled=true;
   document.getElementById("Output2").innerHTML = "";
}



function sum()
{
var n,i, sum = 0;
n = parseInt(document.getElementById ("first").value);
for(i = 1; i <= n; i++)
{
sum = sum+i;
}
document.getElementById ("num").innerHTML="Sum of "+n+ " distinct numbers is :"+sum;
}
